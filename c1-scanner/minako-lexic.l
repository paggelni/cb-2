%option noyywrap
%option yylineno
%option nounput
%option noinput
%{
    /*Definitions*/
int fileno(FILE *stream);
#define AND             257
#define OR              258
#define EQ              259
#define NEQ             260
#define LEQ             261
#define GEQ             262
#define LSS             263
#define GRT             264
#define KW_BOOLEAN      265
#define KW_DO           266
#define KW_ELSE         267
#define KW_FLOAT        268
#define KW_FOR          269
#define KW_IF           270
#define KW_INT          271
#define KW_PRINTF       272
#define KW_RETURN       273
#define KW_VOID         274
#define KW_WHILE        275
#define CONST_INT       276
#define CONST_FLOAT     277
#define CONST_BOOLEAN   278
#define CONST_STRING    279
#define ID              280

    int lines = 1;
%}

DIGIT   [0-9]
LETTER  [a-zA-Z]
INTEGER {DIGIT}+
FLOAT   {INTEGER}"."{INTEGER}|"."{INTEGER}

CONST_INT       {INTEGER}
CONST_FLOAT     {FLOAT}([eE]([-+])?{INTEGER})?|{INTEGER}[eE]([-+])?{INTEGER}
CONST_BOOLEAN   "true"|"false"
CONST_STRING    "\""[^\n\"]*"\""
ID              ({LETTER})+({DIGIT}|{LETTER})*

%x comment

%%
<INITIAL,comment>\n     {++lines;}
 
"/*"                    {BEGIN(comment);}

<comment>[^*\n]*       
<comment>"*"+[^*/\n]*  
<comment>"*"+"/"        {BEGIN(INITIAL);}


bool        {printf("Line: %4d\tToken: %d\n", lines, KW_BOOLEAN);}
do          {printf("Line: %4d\tToken: %d\n", lines, KW_DO);}
else        {printf("Line: %4d\tToken: %d\n", lines, KW_ELSE);}
float       {printf("Line: %4d\tToken: %d\n", lines, KW_FLOAT);}
for         {printf("Line: %4d\tToken: %d\n", lines, KW_FOR);}
if          {printf("Line: %4d\tToken: %d\n", lines, KW_IF);}
int         {printf("Line: %4d\tToken: %d\n", lines, KW_INT);}
printf      {printf("Line: %4d\tToken: %d\n", lines, KW_BOOLEAN);}
return      {printf("Line: %4d\tToken: %d\n", lines, KW_RETURN);}
void        {printf("Line: %4d\tToken: %d\n", lines, KW_VOID);}
while       {printf("Line: %4d\tToken: %d\n", lines, KW_WHILE);}

{CONST_INT}     {printf("Line: %4d\tINT:    %s\n", lines, yytext);}
{CONST_FLOAT}   {printf("Line: %4d\tFLOAT:  %s\n", lines, yytext);}
{CONST_STRING}  {printf("Line: %4d\tSTRING: %s\n", lines, yytext);}
{CONST_BOOLEAN} {printf("Line: %4d\tBOOL:   %s\n", lines, yytext);}
{ID}            {printf("Line: %4d\tID:     %s\n", lines, yytext);}

"+" | "-" | "*" | "/" | "=" | "," | ";" | "(" | ")" | "{" | "}"   {printf("Line: %4d\tToken: '%s'\n", lines, yytext);}
"&&"        {printf("Line: %4d\tToken: %d\n", lines, AND);}
"||"        {printf("Line: %4d\tToken: %d\n", lines, OR);}
"=="        {printf("Line: %4d\tToken: %d\n", lines, EQ);}
"!="        {printf("Line: %4d\tToken: %d\n", lines, NEQ);}
"<="        {printf("Line: %4d\tToken: %d\n", lines, LEQ);}
">="        {printf("Line: %4d\tToken: %d\n", lines, GEQ);}
"<"         {printf("Line: %4d\tToken: %d\n", lines, LSS);}
">"         {printf("Line: %4d\tToken: %d\n", lines, GRT);}


[ \t]+          /*tabs and whitespaces*/

<<EOF>>     {exit(0);}

.      {fprintf(stderr, "Line: %4d\tinvalid argument: %s\n", lines, yytext); exit(-1);}

%%

int main(int argc, char** argv)
{
    ++argv, --argc;
    if(argc > 0)
        yyin = fopen(argv[0], "r");
    else
        yyin = stdin;

    yylex();
}